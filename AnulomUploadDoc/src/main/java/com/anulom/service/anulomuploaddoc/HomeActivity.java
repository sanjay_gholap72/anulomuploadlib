package com.anulom.service.anulomuploaddoc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText edtDocumentID,edtDocumentDescription;
    private Button btnChooseImage;

   String documentID,documentDescription;
   String userMail;
   String activtyName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Bundle bundle = getIntent().getExtras();
        if(bundle !=null)
        {
            userMail = bundle.getString("EmailID");
        }


        Log.wtf("userMailID ",userMail );
        edtDocumentID =(EditText) findViewById(R.id.edtxt_docid);
        edtDocumentDescription = (EditText) findViewById(R.id.edtxt_docdes);
        btnChooseImage = (Button)findViewById(R.id.btn_choose);

        btnChooseImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btnChooseImage)
        {
            ChooseImage();
        }

    }

    private void ChooseImage()
    {
        documentID = edtDocumentID.getText().toString().trim();
        documentDescription =  edtDocumentDescription.getText().toString().trim();
        if(documentID.isEmpty())
        {
            Snackbar.make(findViewById(android.R.id.content),"Document ID can not blank..", Snackbar.LENGTH_SHORT).show();
        }
        else if(documentDescription.isEmpty())
        {
            Snackbar.make(findViewById(android.R.id.content),"Document description can not blank..", Snackbar.LENGTH_SHORT).show();
        }
        else
        {
            if(documentID.contains("_"))
            {
                Log.wtf("documentID",documentID);
                Log.wtf("documentDescription",documentDescription);
                Log.wtf("userMailID ",userMail );

                Intent intent = new Intent(HomeActivity.this, ImageChooser.class);
                intent.putExtra("documentID", documentID);
                intent.putExtra("documentDescription", documentDescription);
                intent.putExtra("EmailID", userMail);
                startActivity(intent);
            }
            else
            {
                Snackbar.make(findViewById(android.R.id.content),"Please check document id", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(HomeActivity.this,AnulomDashboaed.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("EmailID",userMail);
        startActivity(intent);
        finish();
    }

}