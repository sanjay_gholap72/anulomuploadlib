package com.anulom.service.anulomuploaddoc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UploadPdfActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText edtDocumentID,edtDocumentDescription;
    private Button btnChooseImage,btnUploade;
    private TextView txtselectedfile;

    private static final int PICK_IMAGE_REQUEST = 234;//a constant to track the file chooser intent
    private Uri filePath;
    private File path;

    private DatabaseReference databaseReference;
    private StorageReference storage;
    private FirebaseAuth mAuth;;

    private String executorUID,executorName;
    private String documentID,documentDescription;
    private String pushKey,newPushKey;
    private String photoLink;

    private Uri file;
    private String userMail;
    DocumentDetails documentDetails;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_pdf);

        Bundle bundle = getIntent().getExtras();
        if(bundle !=null)
        {
            userMail = bundle.getString("EmailID");
        }



        context = this;

        databaseReference = FirebaseDatabase.getInstance().getReference();
        storage= FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        Log.wtf("userMailID ",userMail );

        edtDocumentID =(EditText) findViewById(R.id.edtxt_docid);
        edtDocumentDescription = (EditText) findViewById(R.id.edtxt_docdes);
        btnChooseImage = (Button)findViewById(R.id.btn_choose);
        btnUploade = (Button) findViewById(R.id.btn_upload);
        txtselectedfile = findViewById(R.id.txt_sltfile);

        btnChooseImage.setOnClickListener(this);
        btnUploade.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btnChooseImage)
        {
            ChooseImage();
        }
        else  if(v == btnUploade)
        {
            UploadData();
        }
    }

    private void UploadData()
    {
        pushKey = databaseReference.push().getKey();
        newPushKey = databaseReference.push().getKey();

        documentID = edtDocumentID.getText().toString().trim();
        documentDescription =  edtDocumentDescription.getText().toString().trim();

        file = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String millisInString  = dateFormat.format(new Date());

        if(documentID.isEmpty())
        {
            Snackbar.make(findViewById(android.R.id.content),"Document ID can not blank..", Snackbar.LENGTH_SHORT).show();
        }
        else if(documentDescription.isEmpty())
        {
            Snackbar.make(findViewById(android.R.id.content),"Document description can not blank..", Snackbar.LENGTH_SHORT).show();
        }
        else if(filePath==null)
        {
            Snackbar.make(findViewById(android.R.id.content),"Uploading file or Image can not blank", Snackbar.LENGTH_SHORT).show();
        }
        else
        {

            if(documentID.contains("_"))
            {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Uploading");
                progressDialog.show();

                StorageReference riversRef = storage.child("Document").child(userMail).child("UploadDocumentKey").child(newPushKey).child(documentID);

                UploadTask uploadTask = riversRef.putFile(filePath);
                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
                {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
                    {
                        progressDialog.dismiss(); //if the upload is successfull  //hiding the progress dialog
                        Toast.makeText(getApplicationContext(), "File Uploaded successfully... ", Toast.LENGTH_LONG).show();

                        Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        task.addOnSuccessListener(new OnSuccessListener<Uri>()
                        {
                            @Override
                            public void onSuccess(Uri uri)
                            {
                                photoLink = uri.toString();

                                documentDetails = new DocumentDetails(documentID,documentDescription,userMail,millisInString,photoLink);
                                databaseReference.child("Executor").child(userMail).child("UploadDocument").child(pushKey).setValue(documentDetails);
                                databaseReference.child("User").child("UploadDocumenDetails").child(pushKey).setValue(documentDetails);

                                Snackbar.make(findViewById(android.R.id.content),"pdf file uplaod", Snackbar.LENGTH_LONG).show();

                                Intent intent = new Intent(UploadPdfActivity.this,AnulomDashboaed.class);
                                intent.putExtra("EmailID",userMail);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception exception)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


                edtDocumentID.setText("");
                edtDocumentDescription.setText("");
                txtselectedfile.setText("No pdf file is selected");

            }
            else
            {
                Snackbar.make(findViewById(android.R.id.content),"Please check document id", Snackbar.LENGTH_SHORT).show();
            }
        }
    }


    private void ChooseImage()
    {
        Intent intent = new Intent();
        intent.setType("application/pdf");;
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"),PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            filePath = data.getData();
            txtselectedfile.setText(filePath.toString());
        }
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(UploadPdfActivity.this,AnulomDashboaed.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("EmailID",userMail);
        startActivity(intent);
        finish();
    }
}