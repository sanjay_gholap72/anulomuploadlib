package com.anulom.service.anulomuploaddoc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class UpDocumentList extends AppCompatActivity implements View.OnClickListener
{

    private DatabaseReference myRef;

    RecyclerView recyclerView;

    private List<DocumentDetailsList> bDocumentDetailsList;
    private DocumentAdapter documentAdapter;

    private String executorKey;
    private String documentID;
    private String documentURL;
    private String date;
    private String userMail;
    private String currentDate;
    private String uploadDate;
    private String documentDescription;
    private Date dateUpload,dateCurrent,beforeCurrentDate;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_document_list);

        Bundle bundle = getIntent().getExtras();

        if(bundle !=null)
        {
            userMail = bundle.getString("EmailID");

        }
        myRef = FirebaseDatabase.getInstance().getReference();
        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());


        floatingActionButton =(FloatingActionButton)findViewById(R.id.fab);
        recyclerView = findViewById(R.id.recycler_document);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        bDocumentDetailsList = new ArrayList<DocumentDetailsList>();
        documentAdapter = new DocumentAdapter(getApplication(),bDocumentDetailsList);
        recyclerView.setAdapter(documentAdapter);
        bDocumentDetailsList.clear();

        AddListData();
        Log.wtf("userMailID ",userMail );

        floatingActionButton.setOnClickListener(this);
    }

    private void AddListData()
    {
        myRef.child("Executor").child(userMail).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.hasChild("UploadDocument"))
                {
                    for(DataSnapshot secondSnapshot : dataSnapshot.child("UploadDocument").getChildren())
                    {
                        executorKey = secondSnapshot.getKey();
                        documentID = String.valueOf(secondSnapshot.child("eDocumentID").getValue());
                        documentURL = String.valueOf((secondSnapshot.child("pdfUrl").getValue()));
                        date = String.valueOf(secondSnapshot.child("date").getValue());
                        documentDescription = String.valueOf(secondSnapshot.child("eDocumentDescription").getValue());

                        DocumentDetails documentDetails = secondSnapshot.getValue(DocumentDetails.class);
                        uploadDate = documentDetails.getDate();
                        try
                        {

                            dateUpload = new SimpleDateFormat("yyyy-MM-dd").parse(uploadDate);
                            dateCurrent = new SimpleDateFormat("yyyy-MM-dd").parse(currentDate);
                        }
                        catch (ParseException e)
                        {
                            e.printStackTrace();
                        }

                        if((dateUpload.compareTo(dateCurrent)>0)||(dateUpload.compareTo(dateCurrent)==0))
                        {
                            bDocumentDetailsList.add(new DocumentDetailsList(executorKey,documentID,documentURL,date,documentDescription));
                        }


                    }

                    recyclerView.setAdapter(documentAdapter);
                }
                else
                {
                    //  no file is upload
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }

    @Override
    public void onClick(View v)
    {
        if(v==floatingActionButton)
        {
            PastData();
        }
    }

    private void PastData()
    {
        bDocumentDetailsList.clear();
        myRef.child("Executor").child(userMail).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.hasChild("UploadDocument"))
                {
                    for(DataSnapshot secondSnapshot : dataSnapshot.child("UploadDocument").getChildren())
                    {
                        executorKey = secondSnapshot.getKey();
                        documentID = String.valueOf(secondSnapshot.child("eDocumentID").getValue());
                        documentURL = String.valueOf((secondSnapshot.child("pdfUrl").getValue()));
                        date = String.valueOf(secondSnapshot.child("date").getValue());
                        documentDescription = String.valueOf(secondSnapshot.child("eDocumentDescription").getValue());

                        bDocumentDetailsList.add(new DocumentDetailsList(executorKey,documentID,documentURL,date,documentDescription));
                    }

                    Collections.reverse(bDocumentDetailsList);
                    recyclerView.setAdapter(documentAdapter);
                }
                else
                {
                    //  no file is upload
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });

    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(UpDocumentList.this,AnulomDashboaed.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("EmailID",userMail);
        startActivity(intent);
        finish();
    }

}